package com.computoplant.hub.scheduler;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.computoplant.hub.ComputoPlantConfiguration;
import com.computoplant.hub.model.AbstractSensorJob;

import io.micrometer.core.instrument.MeterRegistry;

@Component
public class ScheduleExecutor {

	public static final Logger LOG = LoggerFactory.getLogger(ScheduleExecutor.class);

	@Autowired
	private TaskScheduler executor;

	@Autowired
	private ComputoPlantConfiguration computoPlantConfiguration;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private MeterRegistry meterRegistry;

	@EventListener
	private void configureAndRunSchedules(ContextRefreshedEvent event) {
		// SensorJobs
		for (AbstractSensorJob sensorJob : computoPlantConfiguration.getLimitedDurationSensorRegistry()) {
			LOG.info("Starting job: " + sensorJob.getName());
			sensorJob.setRestTemplate(restTemplate);
			sensorJob.setMeterRegistry(meterRegistry);
			executor.scheduleAtFixedRate(sensorJob, Date.from(LocalDateTime.now().plusSeconds(sensorJob.getStartOffset())
					.atZone(ZoneId.systemDefault()).toInstant()), sensorJob.getPollingInterval());
		}

		for (AbstractSensorJob sensorJob : computoPlantConfiguration.getUnlimitedDurationSensorRegistry()) {
			LOG.info("Starting job: " + sensorJob.getName());
			sensorJob.setRestTemplate(restTemplate);
			sensorJob.setMeterRegistry(meterRegistry);
			executor.scheduleAtFixedRate(sensorJob, Date.from(LocalDateTime.now().plusSeconds(sensorJob.getStartOffset())
					.atZone(ZoneId.systemDefault()).toInstant()), sensorJob.getPollingInterval());
		}

		// Scheduled Relay Jobs
	}

}
