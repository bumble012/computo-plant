package com.computoplant.hub;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.computoplant.hub.model.LimitedDurationSensorJob;
import com.computoplant.hub.model.ScheduledRelayJob;
import com.computoplant.hub.model.UnlimitedDurationSensorJob;

@Configuration
@ConfigurationProperties
@EnableConfigurationProperties
public class ComputoPlantConfiguration {

	private List<LimitedDurationSensorJob> limitedDurationSensorRegistry;
	private List<UnlimitedDurationSensorJob> unlimitedDurationSensorRegistry;
	private List<ScheduledRelayJob> scheduledRelayJob;

	public List<LimitedDurationSensorJob> getLimitedDurationSensorRegistry() {
		return limitedDurationSensorRegistry;
	}

	public void setLimitedDurationSensorRegistry(List<LimitedDurationSensorJob> limitedDurationSensorRegistry) {
		this.limitedDurationSensorRegistry = limitedDurationSensorRegistry;
	}

	public List<UnlimitedDurationSensorJob> getUnlimitedDurationSensorRegistry() {
		return unlimitedDurationSensorRegistry;
	}

	public void setUnlimitedDurationSensorRegistry(List<UnlimitedDurationSensorJob> unlimitedDurationSensorRegistry) {
		this.unlimitedDurationSensorRegistry = unlimitedDurationSensorRegistry;
	}

	public List<ScheduledRelayJob> getScheduledRelayJob() {
		return scheduledRelayJob;
	}

	public void setScheduledRelayJob(List<ScheduledRelayJob> scheduledRelayJob) {
		this.scheduledRelayJob = scheduledRelayJob;
	}

}
