package com.computoplant.hub.model;

import java.net.URL;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.computoplant.hub.exception.ConfigurationException;

import io.micrometer.core.instrument.MeterRegistry;

public abstract class AbstractSensorJob implements Runnable {

	public Logger LOG = LoggerFactory.getLogger(this.getClass());

	/**
	 * Unique name identifying the sensor
	 */
	protected String name;

	/**
	 * Address to poll the sensor at (should be a standard rest get interface)
	 */
	protected URL sensorAddress;

	/**
	 * (Optional) Value at which the relay should fire
	 */
	protected Double upperThresholdValue;

	/**
	 * (Required if UpperThresholdValue set) address at which to reach the relay
	 */
	protected URL upperThresholdAddress;

	/**
	 * (Optional) Value at which the relay should fire
	 */
	protected Double lowerThresholdValue;

	/**
	 * (Required if LowerThresholdValue set) address at which to reach the relay
	 */
	protected URL lowerThresholdAddress;

	/**
	 * time period that must expire before the job can run. This should ensure
	 * that two 5-min pollers can run at sperate times
	 */
	protected Long startOffset = 0L;

	/**
	 * the name of the sensor that this is dependant on being 'off' (think ph vs
	 * ec sensor)
	 */
	protected String sensorDependancyName;
	/**
	 * the time period that must elapse before this sensor can trigger after the
	 * dependency has been polled
	 */
	protected Double sensorDependancyCooldown;

	/**
	 * Interval for running the poller
	 */
	protected Long pollingInterval;

	protected RestTemplate restTemplate;
	protected MeterRegistry meterRegistry;

	/**
	 * Optionally configure a sensor dependency. Useful if a given sensor input
	 * needs to wait a period of time before taking action if another action may
	 * have recent run
	 * 
	 * @param sensorDependancyName
	 * @param sensorDependancyCooldown
	 */
	public void configureSensorDependency(@NotNull String sensorDependancyName, @NotNull Double sensorDependancyCooldown) {
		if (sensorDependancyName == null || sensorDependancyCooldown == null) {
			throw new ConfigurationException("SensorDependency configuration incomplete for sensor: " + getSensorAddress());
		}
		this.sensorDependancyCooldown = sensorDependancyCooldown;
		this.sensorDependancyName = sensorDependancyName;
	}

	/**
	 * Performs the principle work of the job. This method will poll the sensor
	 * and take action based on its configuration. The SensorResult argument
	 * will be updated by reference
	 * 
	 * @param sensorResult
	 */
	@Override
	public void run() {
		Double value = pollSensor();
		meterRegistry.gauge(name, value);
		handleResult(value);
	}

	protected abstract void handleResult(Double value);

	/**
	 * Polls a sensor and returns a value as a double
	 */
	private Double pollSensor() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> entity = new HttpEntity<>(headers);
		HttpEntity<Map> response = restTemplate.exchange(
				getSensorAddress().toString(),
				HttpMethod.GET,
				entity,
				Map.class);

		return Double.parseDouble(response.getBody().get("value").toString());
	}

	/**
	 * Performs a GET to a relay endpoint
	 * 
	 * @param relayAddress
	 * @param relayTime
	 */
	protected void activateRelay(String relayAddress) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<?> entity = new HttpEntity<>(headers);
		HttpEntity<Map> response = restTemplate.exchange(
				relayAddress,
				HttpMethod.GET,
				entity,
				Map.class);
	}

	/**
	 * Convenience method to check for upper threshold
	 * 
	 * @return
	 */
	protected Boolean hasUpperThreshold() {
		return getUpperThresholdValue() != null;
	}

	/**
	 * Convenience method to check for lower threshold
	 * 
	 * @return
	 */
	protected Boolean hasLowerThreshold() {
		return getLowerThresholdValue() != null;
	}

	/**
	 * Convenience method to check for sensor dependency
	 * 
	 * @return
	 */
	private Boolean hasSensorDependency() {
		return getSensorDependancyName() != null;
	}

	public URL getSensorAddress() {
		return sensorAddress;
	}

	public void setSensorAddress(URL sensorAddress) {
		this.sensorAddress = sensorAddress;
	}

	public Double getUpperThresholdValue() {
		return upperThresholdValue;
	}

	public void setUpperThresholdValue(Double upperThresholdValue) {
		this.upperThresholdValue = upperThresholdValue;
	}

	public URL getUpperThresholdAddress() {
		return upperThresholdAddress;
	}

	public void setUpperThresholdAddress(URL upperThresholdAddress) {
		this.upperThresholdAddress = upperThresholdAddress;
	}

	public Double getLowerThresholdValue() {
		return lowerThresholdValue;
	}

	public void setLowerThresholdValue(Double lowerThresholdValue) {
		this.lowerThresholdValue = lowerThresholdValue;
	}

	public URL getLowerThresholdAddress() {
		return lowerThresholdAddress;
	}

	public void setLowerThresholdAddress(URL lowerThresholdAddress) {
		this.lowerThresholdAddress = lowerThresholdAddress;
	}

	public Long getStartOffset() {
		return startOffset;
	}

	public void setStartOffset(Long startOffset) {
		this.startOffset = startOffset;
	}

	public String getSensorDependancyName() {
		return sensorDependancyName;
	}

	public void setSensorDependancyName(String sensorDependancyName) {
		this.sensorDependancyName = sensorDependancyName;
	}

	public Double getSensorDependancyCooldown() {
		return sensorDependancyCooldown;
	}

	public void setSensorDependancyCooldown(Double sensorDependancyCooldown) {
		this.sensorDependancyCooldown = sensorDependancyCooldown;
	}

	public Long getPollingInterval() {
		return pollingInterval;
	}

	public void setPollingInterval(Long pollingInterval) {
		this.pollingInterval = pollingInterval;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MeterRegistry getMeterRegistry() {
		return meterRegistry;
	}

	public void setMeterRegistry(MeterRegistry meterRegistry) {
		this.meterRegistry = meterRegistry;
	}

}
