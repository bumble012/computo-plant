from flask import Flask
from flask import Response
from flask import request
from flask_cors import CORS
from i2c import AtlasI2C
import json
import time
import re

app = Flask(__name__)
CORS(app)

phMeter = AtlasI2C(address=99)

@app.route('/')
def index():
    global phMeter, READ_COMMAND
    response_object = {}
    read_output = phMeter.query('R')
    read_output_pattern = re.compile('^Command succeeded (\d+(\.\d+)?)(\x00)+$')
    read_output_match = re.match(read_output_pattern, read_output)
    print(read_output)
    if read_output_match != None:
        print(read_output_match)
        read_output_value = float(read_output_match.group(1))
        response_object = {
            'value': read_output_value
        }
    return Response(json.dumps(response_object), mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8082)
