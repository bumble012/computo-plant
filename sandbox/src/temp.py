import os
import glob
import time
import RPi.GPIO as GPIO

#Config for the 1 pin data (not a real i2c device)
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

#Config for the temp mount location
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

#Config for the indictor LED
GPIO.setmode(GPIO.BCM)
pinOUt = 17
GPIO.setup(pinOUt, GPIO.OUT)

#Read the data from the sensor
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

#Extract the useful temperature data from the sensor and convert it to F
def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_f

#Flicker the LED indictor
def flash_indicator():
    i = 0
    while i < 10:
        state = GPIO.input(pinOUt)
        if(state):
            GPIO.output(pinOUt, False)
        else:
            GPIO.output(pinOUt, True)
        time.sleep(.1)
        i = i + 1

#Infinite loop...  this just runs the program.
while True:
    temp = read_temp()
    print("Temperature = " + str(temp))
    if(temp >= 100):
        flash_indicator()
        print("\t-> Over 100. Indicator active.")
    time.sleep(1)