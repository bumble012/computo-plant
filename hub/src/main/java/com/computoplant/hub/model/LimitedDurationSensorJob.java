package com.computoplant.hub.model;

public class LimitedDurationSensorJob extends AbstractSensorJob {

	/**
	 * (Required if UpperThresholdValue set) amount of time that the relay
	 * should fire for
	 */
	private Long upperRelayTime;

	/**
	 * (Required if UpperThresholdValue set) time period that must expire before
	 * the relay can fire again
	 */
	private Long upperRelayCooldown;

	/**
	 * (Required if LowerThresholdValue set) amount of time that the relay
	 * should fire for
	 */
	private Long lowerRelayTime;

	/**
	 * (Required if LowerThresholdValue set) time period that must expire before
	 * the relay can fire again
	 */
	private Long lowerRelayCooldown;

	@Override
	protected void handleResult(Double value) {
		LOG.info(getName() + " value at " + value);

		if (hasUpperThreshold() && value.compareTo(getUpperThresholdValue()) >= 0) {
			// The upper threshold has been passed
			LOG.info(getName() + " above threshold '" + getUpperThresholdValue() + " firing relay for " + getUpperRelayTime() + " seconds");
			activateRelay(getUpperThresholdAddress().toString() + getUpperRelayTime());
		}

		if (hasLowerThreshold() && value.compareTo(getLowerThresholdValue()) <= 0) {
			// The lower threshold has been passed
			LOG.info(getName() + " below threshold '" + getLowerThresholdValue() + " firing relay for " + getLowerRelayTime() + " seconds");
			activateRelay(getLowerThresholdAddress().toString() + getLowerRelayTime());
		}
	}

	public Long getUpperRelayTime() {
		return upperRelayTime;
	}

	public void setUpperRelayTime(Long upperRelayTime) {
		this.upperRelayTime = upperRelayTime;
	}

	public Long getUpperRelayCooldown() {
		return upperRelayCooldown;
	}

	public void setUpperRelayCooldown(Long upperRelayCooldown) {
		this.upperRelayCooldown = upperRelayCooldown;
	}

	public Long getLowerRelayTime() {
		return lowerRelayTime;
	}

	public void setLowerRelayTime(Long lowerRelayTime) {
		this.lowerRelayTime = lowerRelayTime;
	}

	public Long getLowerRelayCooldown() {
		return lowerRelayCooldown;
	}

	public void setLowerRelayCooldown(Long lowerRelayCooldown) {
		this.lowerRelayCooldown = lowerRelayCooldown;
	}

}
