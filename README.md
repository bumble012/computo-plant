# computo-plant

Proof of concept application designed to monitor indoor garden environment conditions.

- sandbox : test applications written that may have reference value later..?
- temperature-webapp : Application to expose temperature sensor data via a web interface
- relay-webapp : Application to toggle relay state
- hub : Application to expose data and controls for various sensor inputs