package com.computoplant.hub.exception;

/**
 * Simple runtime exception to describe configuration related errors
 * 
 * @author jginnow
 *
 */
public class ConfigurationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6534157659876406226L;

	public ConfigurationException(String message) {
		super(message);
	}

	public ConfigurationException(String message, Throwable e) {
		super(message, e);
	}

}
