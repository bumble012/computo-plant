package com.computoplant.hub.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.computoplant.hub.ComputoPlantConfiguration;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.search.Search;

@Controller
public class DataController {

	@Autowired
	private ComputoPlantConfiguration computoPlantConfiguration;

	@Autowired
	private MeterRegistry meterRegistry;

	@GetMapping(path = "/test")
	public ResponseEntity doTest() {
		Search search = meterRegistry.find("temp1");
		Gauge gauge = search.gauge();
		return new ResponseEntity<String>(gauge.toString(),
				HttpStatus.OK);
	}

}
