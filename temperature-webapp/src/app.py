from flask import Flask
from flask import Response
from tempService import Themometer

app = Flask(__name__)

t = Themometer()

@app.route('/')
def index():
    result = '{"value" : ' + str(t.read_temp()) + "}"
    return Response(result, mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')