from flask import Flask
from flask import Response
from flask import request
from flask_cors import CORS
import time
import RPi.GPIO as GPIO

app = Flask(__name__)
CORS(app)

#Config for the indictor LED
GPIO.setmode(GPIO.BCM)
gpioPinArray = {
    1:6,
    2:12,
    3:13,
    4:19,
    5:16,
    6:26,
    7:20,
    8:21
    }


def setUpPinOut():
    i = 1
    while i < len(gpioPinArray) + 1:
        GPIO.setup(gpioPinArray[i], GPIO.OUT)
        i += 1

def activePinForDuration(pin, upTime):
    state = GPIO.input(pin)
    if(state == False):
        GPIO.output(pin, False)
    GPIO.output(pin, True)
    time.sleep(upTime)
    GPIO.output(pin, False)

def togglePin(pin, turnOn):
    state = GPIO.input(pin)
    if(state != turnOn):
        GPIO.output(pin, turnOn)

@app.route('/relay/<pin>')
def manipulateRelayPin(pin):
    duration = request.args.get('duration')
    toggle = request.args.get('toggle')
    pin = gpioPinArray[int(pin)]
    if pin == None:
        return Response('{"message" : "Pin not mapped" }', mimetype='application/json'), 400
    if duration != None and toggle != None:
        return Response('{"message" : "Toggle and Duration are mutually exclusive" }', mimetype='application/json'), 400
    if duration != None:
        duration = float(duration)
        activePinForDuration(pin,duration)
        result = '{"message" : "Success" }'
    elif toggle != None:
        if toggle == "on":
            togglePin(pin,True)
            result = '{"message" : "Success" }'
        elif toggle == "off":
            togglePin(pin,False)
            result = '{"message" : "Success" }'
        else:
            result = '{"message" : "fail. Unknown action ' + toggle + '" }'
    return Response(result, mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

setUpPinOut()